const express = require("express");
const router = express.Router();
const userControllers = require("../capstone-controllers/userControllers.js");
const auth = require("../auth.js");
const user = require("../../s37/d1/models/user.js");

// Register Routes
router.post("/Register", (request, response) => {
    userControllers.registerUser(request.body)
    .then(resultFromController => response.send(resultFromController))
})

// Getting Profile Routes
router.post("/:usersId/userDetails", auth.verify, (request, response) => {
    
    const gettingDataByAdmin = {
        isAdmin : auth.decode(request.headers.authorization).isAdmin
    }
    
    userControllers.getProfile(request.params.usersId, gettingDataByAdmin)
    .then(resultFromController => 
        response.send(resultFromController))
})

// Login Routes
router.post("/Login", (request, response) => {
    userControllers.loginUser(request.body)
    .then(resultFromController =>
        response.send(resultFromController))
})

// Checkout Routes
router.post("/Checkout", auth.verify, userControllers.checkOut);

// for Delivery
router.get("/Delivery", auth.verify, userControllers.deliverOrder);

module.exports = router;