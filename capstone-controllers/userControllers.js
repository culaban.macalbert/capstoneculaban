const Users = require("../capstone-models/users.js");
const Products = require("../capstone-models/products.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const { update } = require("../capstone-models/users.js");
const { response } = require("express");
const users = require("../capstone-models/users.js");

// Registration
module.exports.registerUser = async (reqBody) => {

    const existingUser = await Users.findOne({email: reqBody.email});
    
    if (existingUser){
        return "Email already Exists";
    }

    const newUser = new Users ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo,
    })

    return newUser.save()
    .then((Users, error) => 
    {
        if(error){
            return "Please Try Again";
        }else{
            return "Registered Successfully!";
        }
    })
}

// Login
module.exports.loginUser = (requestBody) => {
    return Users.findOne({email: requestBody.email})
    .then(result => {
        if(result == null){
            return 'Account not Found'
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);

            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)};
            }
            else{
                return "Incorrect Password"
            }
        }
    })
}

// Getting Profile in Database
module.exports.getProfile = (usersId, gettingDataByAdmin) => {
    if(gettingDataByAdmin.isAdmin == true){
	return Users.findById(usersId)
    .then((result, err) => {
		if(err){
			    return false;
		    }
            else{
                result.password = '*****';
                return result;
            }
        })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// Non Admin User Checkout
module.exports.checkOut = async (request, response) => {

    const usersData = auth.decode(request.headers.authorization);

    let productToCart = await Products.findById(request.body.productId)
    .then(result => result);

    let newOrder = {
        usersId: usersData.id,
        email: usersData.email,
        totalAmount: productToCart.price * (request.body.quantity || 1),
        productId: productToCart,
        productName: productToCart.name,
        quantity: request.body.quantity || 1, // default order quantity
        stocks: productToCart.stocks,
        isAdmin: usersData.isAdmin,
        price: productToCart.price
    }


    let isUserUpdated = await Users.findById(newOrder.usersId)
    .then(user => {
        if(newOrder.isAdmin == true){
            response.status(404).json("Only customers can order Products");
        }else {
            user.myOrders.push({
                products: [
                    {
                        productName: newOrder.productName,
                        quantity: newOrder.quantity || 1 ,// default order quantity
                        price: newOrder.price
                    },
                ],
                totalAmount: newOrder.totalAmount,
            });
            return user.save()
            .then(result => {
                return true;
            })
            .catch(error => {
                return error;
            })
    }
})
    .catch(error => {
        return error;
    });

    console.log(isUserUpdated); // dapat mag True if Successful


    let isProductUpdated = await Products.findById(newOrder.productId)
        
        .then(product => {

            if(newOrder.isAdmin == true){
                response.status(404).json("Only customers can order Products");
                }else{
                            product.orderDetails.push({
                                userEmail: newOrder.email,
                                productName: newOrder.name,
                                quantity: newOrder.quantity || 1 // default order quantity
                            })

                            if(productToCart.stocks <= 0){
                                response.status(404).send("Out of Stock");
                            }
                            else if (productToCart.stocks < newOrder.quantity)
                                {
                                response.status(404).send("Not Enough Stocks for your request");
                            }
                            else{
                                    product.stocks -= newOrder.quantity;
                                    return product.save()
                                        .then(result => {
                                            return true;
                                        })
                                        .catch(error => {
                                            return error;
                                    })
                                }
                            }
            })
    .catch(error => {
        return false;
    });
    
        console.log(isProductUpdated); // Dapat mag True if Successful

        (isUserUpdated == true && isProductUpdated == true)?
        response.status(200).send("Thanks for Purchasing!") : response.send(false);
}

// Erase Orders for Final Check Out
module.exports.deliverOrder = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let orderData = {
        userId: userData.id,
        email: userData.email
    }

    await users.findById(orderData.userId)
    .then(result => {
        result.myOrders.splice(0, result.myOrders.length);
        result.save();
        res.send("Done! Please proceed to payment")
    })
    .catch(er => console.log(err))
}