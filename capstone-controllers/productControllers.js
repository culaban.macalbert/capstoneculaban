const mongoose = require("mongoose");
const Products = require("../capstone-models/products.js");

// Creating a Product
module.exports.createProduct = (requestBody, createProductByAdmin) => {
    if(createProductByAdmin.isAdmin == true){

        let newProduct = new Products ({
            name: requestBody.name,
            description: requestBody.description,
            price: requestBody.price,
            stocks: requestBody.stocks,
            isActive: requestBody.isActive
        })

        return newProduct.save()
        .then((newProduct, error) =>
        {
            if(error){
                return error;
            }
            else{
                return newProduct;
            }
        })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }
}

// Getting All Products
module.exports.getAllProducts = () => {
    return Products.find({isActive: true}) // Only Active products
    .then (result => {
        return result;
    })
}

// Retrieving Single Product
module.exports.getProduct = (productId) => {
    return Products.findById(productId)
    .then((result, error) => {
        if(error){
            return "Product Not Found";
        }
        else{
            return result;
        }
    })
}

// Update Products via Admin User
module.exports.updateProducts = (productId, updateProductData) => {
    if(updateProductData.isAdmin == true){
        return Products.findByIdAndUpdate(productId,
            {
                name: updateProductData.Products.name,
                description: updateProductData.Products.description,
                price: updateProductData.Products.price,
                stocks: updateProductData.Products.stocks,
                isActive: updateProductData.Products.isActive
            })
            .then((result, error) => {
                if(error){
                    return false;
                }
                return "Successfully Updated the Specific Product!";
            })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
    // Additional Next time: Can't Update Since Same Data 
}


// Product Archive by Admin User
module.exports.archiveProduct = (productId, productArchiveByAdmin) => {
    if(productArchiveByAdmin.isAdmin == true){
        return Products.findByIdAndUpdate(productId,
            {
                isActive: false
            })
            .then((result, error) => {
                if(error){
                    return "Invalid Product Id"
                }
                return "Successfully Archiving a Product";
            })
    }
    else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }
}

