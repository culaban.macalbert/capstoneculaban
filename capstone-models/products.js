const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product Name is Required"]
    },
    description: {
        type: String,
        required: [true, "Description is Required"]
    },
    price: {
        type: Number,
        required: [true, "Price is Required"]
    },
    stocks: {
        type: Number,
        required: [true, "Stocks is Required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orderDetails: [
        {
            userEmail: {
                type: String,
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is Required"]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Products", productSchema);