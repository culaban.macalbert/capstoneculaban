const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    email: {
        type: String,
        required: [true, "Email is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile Number is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    myOrders: [
        {
            totalAmount: {
                type: Number
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            },

            products: [
                {
                    productName: {
                        type: String
                    },
                    quantity: {
                        type: Number,
                        required: [true, "Quantity is Required"]
                    }
                }
            ]
        }
    ]
})

module.exports = mongoose.model("User", userSchema);